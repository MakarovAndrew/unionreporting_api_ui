package tests;

import aquality.selenium.core.logging.Logger;
import forms.AddProjectForm;
import forms.ProjectForm;
import forms.ProjectsForm;
import models.Attachment;
import models.Log;
import models.Project;
import models.TestResult;
import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import unionReportingApiUtils.TestApiUtils;
import unionReportingApiUtils.TokenApiUtils;
import utils.Base64Utils;
import utils.JsonUtils;
import utils.SortingUtils;
import utils.UrlBuilder;
import java.util.List;
import static aquality.selenium.browser.AqualityServices.*;
import static utils.ConfigManager.*;

public class UnionReportingTest {
    private static final String AUTHORIZED_URL = String.valueOf(UrlBuilder.getFullUrl(testConfig, credentials));
    private static final String SESSION_ID = String.valueOf(System.currentTimeMillis());

    @AfterTest
    public void afterTest() {
        getBrowser().quit();
    }

    @Test
    public void checkSendingUnionReportingApiRequestsCombinedWithUiHandling() {
        final SoftAssert softAssert = new SoftAssert();

        Logger.getInstance().info("Performing STEP 1: [API] Get token according variant number");
            final String token = TokenApiUtils.getToken(testData.getVariant());
            Assert.assertFalse(token.isBlank(),
                    "Token was not generated!");

        Logger.getInstance().info("Performing STEP 2: [UI] Navigate to the main page, pass an authorization, send a generated token as a cookie and refresh the page");
            getBrowser().maximize();
            getBrowser().goTo(AUTHORIZED_URL);
            final ProjectsForm projectsForm = new ProjectsForm();
            softAssert.assertTrue(projectsForm.waitForDisplayed(),
                    "Projects form has not opened!");
            getBrowser().getDriver().manage().addCookie(new Cookie("token", token));
            getBrowser().refresh();
            softAssert.assertEquals(projectsForm.getVariantNumberFromFooter(), testData.getVariant(),
                    String.format(
                            "The Variant number from Footer doesn't match task Variant!\nExpected: '%s'\nActual  : '%s'",
                            testData.getVariant(),
                            projectsForm.getVariantNumberFromFooter()
                    )
            );
            softAssert.assertAll();

        Logger.getInstance().info("Performing STEP 3: [UI + API] Navigate to the project page, get list of related tests using API request");
            final Project project = new Project(testData.getProjectName());
            project.setProjectId(projectsForm.getProjectId(project));
            final String projectTestResults = TestApiUtils.getAllTestResultsFromProject(project);
            Assert.assertTrue(JsonUtils.isJsonString(projectTestResults),
                    "API has returned non-json body!");
            projectsForm.clickProjectLabel(project);
            final ProjectForm projectForm = new ProjectForm(project);
            softAssert.assertTrue(projectForm.waitForDisplayed(),
                    "Project form has not opened!");
            final List<TestResult> testResultsFromProjectPage = projectForm.getTestResultsFromProjectPage();
            softAssert.assertTrue(SortingUtils.isSortedDescending(testResultsFromProjectPage, TestResult::getStartTime),
                    "Test results from project page is not sorted by start time in descending order!");
            final List<TestResult> projectTestResultsList = JsonUtils.readObjectsFromJsonString(projectTestResults, TestResult.class);
            softAssert.assertTrue(projectTestResultsList.containsAll(testResultsFromProjectPage),
                    "API response is not contains all test results from project page!");
            softAssert.assertAll();

        Logger.getInstance().info("Performing STEP 4: [UI] Navigate back to the projects page, add new project and refresh page");
            getBrowser().goBack();
            projectsForm.clickAddProjectButton();
            getBrowser().tabs().switchToLastTab();
            final String newTabHandle = getBrowser().tabs().getCurrentTabHandle();
            final AddProjectForm addProjectForm = new AddProjectForm();
            final Project newProject = new Project(testData.getNewProjectName());
            addProjectForm.sendProjectName(newProject);
            addProjectForm.clickSubmitButton();
            softAssert.assertTrue(addProjectForm.isSuccessMessageExist(),
                    "Project has not saved successfully!");
            getBrowser().tabs().closeTab();
            getBrowser().tabs().switchToLastTab();
            softAssert.assertFalse(getBrowser().tabs().getTabHandles().contains(newTabHandle),
                    "Add Project tab has still opened!");
            getBrowser().refresh();
            softAssert.assertTrue(projectsForm.isProjectLabelExist(newProject),
                    "Project has not found in the project list!");
            softAssert.assertAll();

        Logger.getInstance().info("Performing STEP 5: [UI + API] Navigate to the new project page, add test using API (with log and screenshot)");
            newProject.setProjectId(projectsForm.getProjectId(newProject));
            projectsForm.clickProjectLabel(newProject);
            final ProjectForm newProjectForm = new ProjectForm(newProject);
            softAssert.assertTrue(newProjectForm.waitForDisplayed(),
                    "New project form has not opened!");
            final String NEW_TEST_ID = TestApiUtils.addTestResultToProject(newProject, SESSION_ID, newTestResult);
            final Log newTestResultLog = new Log(
                    testData.getNewTestLog(),
                    NEW_TEST_ID
            );
            TestApiUtils.addLogToTestResult(newTestResultLog);
            final Attachment newTestResultAttachment = new Attachment(
                    "image/png",
                    Base64Utils.byteToBase64String(getBrowser().getScreenshot()),
                    NEW_TEST_ID
            );
            TestApiUtils.addAttachmentToTestResult(newTestResultAttachment);
            softAssert.assertTrue(newProjectForm.isTestResultDisplayed(NEW_TEST_ID),
                    "New testresult has not displayed!");
            softAssert.assertAll();
    }
}