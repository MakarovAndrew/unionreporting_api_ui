package models;

import lombok.Getter;

@Getter
public class Log {
    private String content;
    private String testId;

    public Log(String content, String testId) {
        this.content = content;
        this.testId = testId;
    }
}