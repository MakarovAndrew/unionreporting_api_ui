package models;

import lombok.*;

@Getter @NoArgsConstructor @EqualsAndHashCode @ToString
public class TestResult {
    private String duration;
    private String method;
    private String name;
    private String startTime;
    private String endTime;
    private String status;
    private String env;
    private String browser;

    public String getStatus() {
            return status != null ? status.toUpperCase() : null;
    }

    public TestResult(String duration, String method, String name, String startTime, String endTime, String status) {
        this.duration = duration;
        this.method = method;
        this.name = name;
        this.startTime = startTime;
        if (!endTime.isBlank()) this.endTime = endTime;
        this.status = status.toUpperCase();
    }
}