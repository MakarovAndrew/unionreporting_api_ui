package utils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class SortingUtils {

    public static <T, R extends Comparable<? super R>> boolean isSortedDescending(List<T> list, Function<T, R> function) {
        Comparator<T> comparator = Comparator.comparing(function).reversed();
        for (int i = 0; i < list.size() - 1; ++i) {
            T first = list.get(i);
            T next = list.get(i + 1);
            if (comparator.compare(first, next) >= 0) {
                return false;
            }
        }
        return true;
    }
}