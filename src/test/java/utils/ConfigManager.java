package utils;

import models.Credentials;
import models.TestResult;
import models.UrlModel;
import models.TestData;
import java.nio.file.Paths;

public class ConfigManager {
    private final static String PATH_TO_RESOURCES = "./src/test/resources/";
    public final static TestData testData = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "testData.json"), TestData.class);
    public final static Credentials credentials = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "credentials.json"), Credentials.class);
    public final static TestResult newTestResult = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "anyTestResult.json"), TestResult.class);
    public final static UrlModel testConfig = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "testConfig.json"), UrlModel.class);
    public final static UrlModel unionReportingApiConfig = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "unionReportingApiConfig.json"), UrlModel.class);
}