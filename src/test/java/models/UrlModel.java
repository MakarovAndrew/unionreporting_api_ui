package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class UrlModel {
    private String scheme;
    private String host;
    private String pathSegments;
    private int port;
}