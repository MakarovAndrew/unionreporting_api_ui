package unionReportingApiUtils;

import okhttp3.HttpUrl;
import utils.HttpUtils;
import java.util.Map;
import static utils.UrlBuilder.*;
import static utils.ConfigManager.unionReportingApiConfig;

public class TokenApiUtils {

    public static String getToken(String variant) {
        final Map<String,String> queryParameters = Map.of(
                "variant", variant
        );
        final HttpUrl url = getFullUrl(unionReportingApiConfig,"token/get", queryParameters);
        return HttpUtils.sendPostRequest(url);
    }
}