package models;

import lombok.Getter;

@Getter
public class Attachment {
    private String contentType;
    private String content;
    private String testId;

    public Attachment(String contentType, String content, String testId) {
        this.contentType = contentType;
        this.content = content;
        this.testId = testId;
    }
}