package forms;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.ILabel;
import models.Project;
import models.TestResult;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

public class ProjectForm extends BaseForm{

    public ProjectForm(Project project) {
        super(
                By.xpath(
                        String.format("//a[@href='allTests?projectId=%s' and text()!='All running tests ()']", project.getProjectId())
                ), String.format("Project '%s' page", project.getProjectName())
        );
    }

    public boolean isTestResultDisplayed(String testId) {
        return getElementFactory().getLabel(
                By.xpath(
                    String.format("//a[@href='testInfo?testId=%s']", testId)
                ), String.format("Testresult#%s", testId)
        ).state().waitForDisplayed();
    }

    public List<TestResult> getTestResultsFromProjectPage() {
        final By durationCellLocator = By.xpath("./td[6]");
        final By methodNameCellLocator = By.xpath("./td[2]");
        final By testNameCellLocator = By.xpath("./td[1]");
        final By startTimeCellLocator = By.xpath("./td[4]");
        final By endTimeCellLocator = By.xpath("./td[5]");
        final By statusCellLocator = By.xpath("./td[3]");
        final List<ILabel> list = getElementFactory().findElements(By.xpath("//table[@id='allTests']/tbody//following-sibling::tr"), ILabel.class);
        final List<TestResult> parsedList = new ArrayList<>();
        for (ILabel listElement: list) {
            TestResult testResult = new TestResult(
                    getTextFromCellLabel(listElement, durationCellLocator),
                    getTextFromCellLabel(listElement, methodNameCellLocator),
                    getTextFromCellLabel(listElement, testNameCellLocator),
                    getTextFromCellLabel(listElement, startTimeCellLocator),
                    getTextFromCellLabel(listElement, endTimeCellLocator),
                    getTextFromCellLabel(listElement, statusCellLocator)
            );
            parsedList.add(testResult);
        }
        return parsedList;
    }

    private String getTextFromCellLabel(ILabel listElement, By locator) {
        return listElement.findChildElement(locator, ElementType.LABEL).getText();
    }
}