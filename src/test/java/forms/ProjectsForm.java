package forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import models.Project;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

public class ProjectsForm extends BaseForm{
    private final IButton addProjectButton = getElementFactory().getButton(By.xpath("//a[@href='addProject']"), "Add Project");

    public ProjectsForm() {
        super(By.xpath("//div[@class='list-group']"), "Projects page");
    }

    public String getVariantNumberFromFooter() {
        return StringUtils.substringAfter(
                getElementFactory().getLabel(
                        By.xpath("//p[contains(@class,'footer-text')]/span"), "Footer"
                ).getText(), "Version: "
        );
    }

    public String getProjectId(Project project) {
        return StringUtils.substringAfter(
                findProjectLabel(project).getAttribute("href"), "projectId="
        );
    }

    public void clickProjectLabel(Project project) {
        findProjectLabel(project).click();
    }

    public void clickAddProjectButton() {
        addProjectButton.click();
    }

    public boolean isProjectLabelExist(Project project) {
        return findProjectLabel(project).state().isDisplayed();
    }

    private ILabel findProjectLabel(Project project) {
        return  getElementFactory().getLabel(
                By.xpath(
                        String.format("//a[text()='%s']", project.getProjectName())
                ), String.format("The %s project", project.getProjectName())
        );
    }
}