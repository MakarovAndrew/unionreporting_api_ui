package utils;

import aquality.selenium.core.logging.Logger;
import okhttp3.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class HttpUtils {
    private final static OkHttpClient client = new OkHttpClient.Builder()
            .protocols(List.of(Protocol.HTTP_1_1))
            .build();

    public static String sendPostRequest(HttpUrl url) {
        Logger.getInstance().info("Sending HTTP POST request :: '%s' ...", url);
        RequestBody emptyBody = RequestBody.create(new byte[0]);
        return sendPostRequest(url, emptyBody);
    }

    public static void sendPostRequest(HttpUrl url, Map<String, String> bodyParameters) {
        Logger.getInstance().info("Sending HTTP POST request with body :: '%s' ...", url);
        FormBody.Builder builder = new FormBody.Builder();
        if (bodyParameters != null) {
            for (Map.Entry<String, String> entry : bodyParameters.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        sendPostRequest(url, builder.build());
    }

    private static String sendPostRequest(HttpUrl url, RequestBody body) {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try {
            return client.newCall(request).execute().body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}