package utils;

import models.Credentials;
import models.UrlModel;
import okhttp3.HttpUrl;
import java.util.Map;

public class UrlBuilder {

    public static HttpUrl getFullUrl(UrlModel urlModel, String endpoint, Map<String, String> parameters) {
        HttpUrl.Builder url = getUrlBasis(urlModel)
                .addPathSegments(endpoint);
        if (parameters != null) {
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                url.addQueryParameter(entry.getKey(), entry.getValue());
            }
        }
        return url.build();
    }

    public static HttpUrl getFullUrl(UrlModel urlModel, Credentials credentials) {
        return getUrlBasis(urlModel)
                .username(credentials.getLogin())
                .password(credentials.getPassword())
                .build();
    }

    private static HttpUrl.Builder getUrlBasis(UrlModel urlModel) {
        return new HttpUrl.Builder()
                .scheme(urlModel.getScheme())
                .host(urlModel.getHost())
                .port(urlModel.getPort())
                .addPathSegments(urlModel.getPathSegments());
    }
}