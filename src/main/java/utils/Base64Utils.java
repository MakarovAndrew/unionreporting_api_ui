package utils;

import org.apache.hc.client5.http.utils.Base64;

public class Base64Utils {

    public static String byteToBase64String(byte[] byteArray) {
        Base64 base64 = new Base64();
        return new String(base64.encode(byteArray));
    }
}