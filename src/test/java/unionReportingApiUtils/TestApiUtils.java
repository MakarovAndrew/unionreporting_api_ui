package unionReportingApiUtils;

import models.Attachment;
import models.Log;
import models.Project;
import models.TestResult;
import okhttp3.HttpUrl;
import utils.HttpUtils;
import java.util.Map;
import static utils.ConfigManager.unionReportingApiConfig;
import static utils.UrlBuilder.getFullUrl;

public class TestApiUtils {

    public static String getAllTestResultsFromProject(Project project) {
        final Map<String,String> queryParameters = Map.of(
                "projectId", project.getProjectId()
        );
        final HttpUrl url = getFullUrl(unionReportingApiConfig,"test/get/json", queryParameters);
        return HttpUtils.sendPostRequest(url);
    }

    public static String addTestResultToProject(Project project, String sid, TestResult testResult) {
        final Map<String,String> queryParameters = Map.of(
                "SID",sid,
                "projectName", project.getProjectName(),
                "testName", testResult.getName(),
                "methodName", testResult.getMethod(),
                "env", testResult.getEnv(),
                "browser", testResult.getBrowser()
        );
        final HttpUrl url = getFullUrl(unionReportingApiConfig,"test/put", queryParameters);
        return HttpUtils.sendPostRequest(url);
    }

    public static void addLogToTestResult(Log log) {
        final Map<String,String> queryParameters = Map.of(
                "testId", log.getTestId(),
                "content", log.getContent()
        );
        final HttpUrl url = getFullUrl(unionReportingApiConfig,"test/put/log", queryParameters);
        HttpUtils.sendPostRequest(url);
    }

    public static void addAttachmentToTestResult(Attachment attachment) {
        final Map<String,String> bodyParameters = Map.of(
                "testId", attachment.getTestId(),
                "content", attachment.getContent(),
                "contentType", attachment.getContentType()
        );
        final HttpUrl url = getFullUrl(unionReportingApiConfig,"test/put/attachment", null);
        HttpUtils.sendPostRequest(url, bodyParameters);
    }
}