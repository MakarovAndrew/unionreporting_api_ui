package utils;

import aquality.selenium.core.logging.Logger;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class JsonUtils {
    private static final ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS);

    public static boolean isJsonString(String string) {
        try {
            mapper.readTree(string);
        } catch (JacksonException e) {
            return false;
        }
        return true;
    }

    public static <T> List<T> readObjectsFromJsonString(String jsonString, Class<T> clazz) {
        try {
            Logger.getInstance().info("Reading '%s' list from the JSON string...", clazz.getSimpleName());
            return mapper.readValue(jsonString, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readObjectFromJsonFile(Path pathToJson, Class<T> clazz) {
        try {
            Logger.getInstance().info("Reading '%s' from file :: '%s' ...", clazz.getSimpleName(), pathToJson);
            return mapper.readValue(pathToJson.toFile(), clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}