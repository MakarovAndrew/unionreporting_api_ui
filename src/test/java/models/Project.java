package models;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class Project {
    private String projectName;
    private String projectId;

    public Project(String projectName) {
        this.projectName = projectName;
    }
}