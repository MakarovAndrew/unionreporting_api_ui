package forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import models.Project;
import org.openqa.selenium.By;

public class AddProjectForm extends BaseForm {
    private final ILabel projectNameInputBox = getElementFactory().getLabel(By.xpath("//input[@id='projectName']"), "Project name");
    private final IButton submitButton = getElementFactory().getButton(By.xpath("//button[@type='submit']"), "Submit");

    public AddProjectForm() {
        super(By.xpath("//form[@id='addProjectForm']"), "Add Project form");
    }

    public void sendProjectName(Project project) {
        projectNameInputBox.sendKeys(project.getProjectName());
    }

    public void clickSubmitButton() {
        submitButton.click();
    }

    public boolean isSuccessMessageExist() {
        return getElementFactory().getLabel(By.xpath("//div[contains(@class,'alert-success')]"), "Success")
                .state().waitForDisplayed();
    }
}